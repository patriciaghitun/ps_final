package com.example.ps_proiect2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ps_proiect2.Model.Ingredient;
import com.example.ps_proiect2.Service.IngredientService;
import com.example.ps_proiect2.controller.IngredientController;
import com.example.ps_proiect2.controller.RecipeController;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserActivity extends AppCompatActivity {
    Button showRecipes;
    Button cookRecipe;

    IngredientController ingredientController = new IngredientController();
    RecipeController recipeController= new RecipeController();

    List<Ingredient> allIngredients;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_activity);

        showRecipes=findViewById(R.id.show_recipes);
        showRecipes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //final String name = ((EditText)(findViewById(R.id.ingredient_edit_text))).getText().toString().trim();

                final Integer idIngredient = Integer.parseInt(((EditText)(findViewById(R.id.ingredient_edit_text))).getText().toString().trim());

                Call<List<Ingredient>> ingredients = IngredientService.getInstance().getIngredients();
                ingredients.enqueue(new Callback<List<Ingredient>>() {
                    @Override
                    public void onResponse(Call<List<Ingredient>> call, Response<List<Ingredient>> response) {
                        if (response.isSuccessful()) {
                            Log.d("Response", response.body().toString());
                            Log.d("AICI","response la lista de ing");

                            for(Ingredient ingredient : response.body()){

                                //if(ingredient.getName().equals(name)){
                                if(ingredient.getIdIngredient() == idIngredient){
                                    Toast.makeText(UserActivity.this,"Ingredientul dat "+ingredient.getIdIngredient(),Toast.LENGTH_LONG).show();

                                    final int idCurrentIngredient = ingredient.getIdIngredient();
                                    //get its recipe
                                    Log.d("AICI","Id ingredient cerut este:"+idCurrentIngredient);

                                    Call<Integer> integerCall=IngredientService.getInstance().getRecipeIdFromIngredientId(idCurrentIngredient);
                                    integerCall.enqueue(new Callback<Integer>() {
                                        @Override
                                        public void onResponse(Call<Integer> call, Response<Integer> response) {
                                            Log.d("ING","SUCCESS");
                                            Log.d("RECIPE ID","ID IS :"+response.body().toString());

                                            int recipeId=response.body();
                                            recipeController.getRecipeById(UserActivity.this,recipeId,listView);

                                        }

                                        @Override
                                        public void onFailure(Call<Integer> call, Throwable t) {
                                            Log.d("ING","FAIL");
                                        }
                                    });
                                }
                            }
                        } else {
                            Log.d("Response", "The code is " + response.code());
                        }
                    }
                    @Override
                    public void onFailure(Call<List<Ingredient>> call, Throwable t) {
                        Log.e("Response", "Error in call", t);
                    }
                });


            }
        });


        cookRecipe=findViewById(R.id.cook_recipe);
        cookRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String recipeIdString = ((EditText)findViewById(R.id.cook_recipe_id)).getText().toString().trim();
                Integer recipeId=Integer.parseInt(recipeIdString);
                Log.d("COOK","Recipe id"+recipeId);
                ingredientController.cookRecipe(UserActivity.this,recipeId);
            }
        });


        listView = (ListView) findViewById(R.id.recipe_list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                Log.d("POSTION", " " + position);
                Integer idCookie = position + 1;
                Toast.makeText(UserActivity.this,"Am selectat "+idCookie,Toast.LENGTH_LONG).show();
                //cook this
            }

        });

    }


}
