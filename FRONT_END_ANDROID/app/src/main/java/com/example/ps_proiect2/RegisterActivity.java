package com.example.ps_proiect2;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ps_proiect2.Model.Person;
import com.example.ps_proiect2.controller.LoginController;

public class RegisterActivity extends Activity {
    Button signUp;
    LoginController loginController = new LoginController();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        signUp=findViewById(R.id.register_button);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String role = ((EditText)findViewById(R.id.register_role)).getText().toString().trim();
                Person person = PersonFactory.createPerson(role);

                String username=((EditText)findViewById(R.id.register_name)).getText().toString().trim();
                String password = ((EditText)findViewById(R.id.register_password)).getText().toString().trim();

                person.setUsername(username);
                person.setPassword(password);

                loginController.register(RegisterActivity.this, person);
            }
        });

    }


}
