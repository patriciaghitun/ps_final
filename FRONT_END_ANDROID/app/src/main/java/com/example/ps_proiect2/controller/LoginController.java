package com.example.ps_proiect2.controller;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.example.ps_proiect2.ChefActivity;
import com.example.ps_proiect2.MainActivity;
import com.example.ps_proiect2.Model.Person;
import com.example.ps_proiect2.Service.PersonService;
import com.example.ps_proiect2.UserActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginController {


    public void getUser(final Context context, String username, String password) {
        Call<Person> user = PersonService.getInstance().login(username, password, "user");
        user.enqueue(new Callback<Person>() {

            @Override
            public void onResponse(Call<Person> call, Response<Person> response) {
                if (response.isSuccessful()) {
                    Log.d("LOGIN", "Success");
                    Toast.makeText(context, "Success!", Toast.LENGTH_SHORT).show();

                    Intent main = new Intent(context, UserActivity.class);
                    context.startActivity(main);
                } else {
                    Log.d("LOGIN", "Fail");
                    Toast.makeText(context, "wrong credentials", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Person> call, Throwable t) {
                Log.d("LOGIN", "Failure");
            }
        });
    }

    public void getChef(final Context context, String username, String password) {
        Call<Person> chef = PersonService.getInstance().login(username, password, "chef");
        chef.enqueue(new Callback<Person>() {

            @Override
            public void onResponse(Call<Person> call, Response<Person> response) {

                if (response.isSuccessful()) {
                    Log.d("LOGIN", "Success");
                    Toast.makeText(context, "Success!", Toast.LENGTH_SHORT).show();

                    Intent main = new Intent(context, ChefActivity.class);
                    context.startActivity(main);
                } else {
                    Log.d("LOGIN", "Fail");
                    Toast.makeText(context, "wrong credentials", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Person> call, Throwable t) {

            }
        });
    }

    public void register(final Context context, Person newPerson) {
        Call<Person> user = PersonService.getInstance().addNewPerson(newPerson);

        user.enqueue(new Callback<Person>() {

            @Override
            public void onResponse(Call<Person> call, Response<Person> response) {
                if (response.isSuccessful()) {
                    Log.d("REGISTER", "Success. Please login now!");
                    Toast.makeText(context, "Success!", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("REGISTER", "Fail");
                    Toast.makeText(context, "wrong credentials", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Person> call, Throwable t) {

            }
        });
    }
}
