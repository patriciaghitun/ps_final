package com.example.ps_proiect2.Service;

import com.example.ps_proiect2.Model.Person;
import com.example.ps_proiect2.Model.Recipe;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RecipeApi {
    @GET("/recipe/all")
    Call<List<Recipe>> getRecipes();

    @Headers("Accept: application/json")
    @POST("/recipe/add")
    Call<Recipe> addNewRecipe(@Body Recipe recipe);


    @GET("/recipe/deleteByGivenId/{idrecipe}")
    Call<Void> deleteByGivenId(@Path("idrecipe") Integer idrecipe);
}
