package com.example.ps_proiect2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.ps_proiect2.controller.LoginController;

public class LoginActivity extends AppCompatActivity {
    EditText username,password;
    Button userBtn;
    Button chefBtn;
    Button registerBtn;

    LoginController loginController = new LoginController();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = (EditText) findViewById(R.id.username_edit_text);
        password = (EditText) findViewById(R.id.passwrod_edit_text);
        userBtn = (Button) findViewById(R.id.user_button);
        chefBtn = (Button) findViewById(R.id.chef_button);
        registerBtn = (Button) findViewById(R.id.register_button);

        userBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Credentials",username.getText().toString()+ password.getText().toString());
                loginController.getUser(LoginActivity.this, username.getText().toString(),password.getText().toString());
            }
        });

        chefBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Credentials",username.getText().toString()+ password.getText().toString());
                loginController.getChef(LoginActivity.this, username.getText().toString(),password.getText().toString());
            }
        });

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("REGISTER", "Register window");
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

    }

}
