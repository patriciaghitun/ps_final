package com.example.ps_proiect2.Service;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RecipeService {

    //private final static String API_URL = "http://192.168.0.101:8080/";
    private final static String API_URL = "http://10.132.2.23:8080/";

    private static RecipeApi recipeApi;

    //get la instanta de retrofit
    public static RecipeApi getInstance() {
        if (recipeApi == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(provideOkHttp())
                    .build();

            recipeApi = retrofit.create(RecipeApi.class);
        }
        return recipeApi;
    }

    private static OkHttpClient provideOkHttp() {
        OkHttpClient.Builder httpBuilder = new OkHttpClient.Builder();
        httpBuilder.connectTimeout(30, TimeUnit.SECONDS);

        return httpBuilder.build();
    }
}
