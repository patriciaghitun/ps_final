package com.example.ps_proiect2.Model;

import com.google.gson.annotations.SerializedName;

public class Ingredient {

    @SerializedName("idingredient")
    private Integer idIngredient;
    @SerializedName("name")
    private String name;
    @SerializedName("stock")
    private int stock;
    @SerializedName("type")
    private String type;
    @SerializedName("idrecipe")
    private int idRecipe;
    @SerializedName("quantity")
    private int quantity;

    public Ingredient(){}

    public Ingredient(Integer idIngredient, String name, int stock, String type, int idRecipe, int quantity) {
        this.idIngredient = idIngredient;
        this.name = name;
        this.stock = stock;
        this.type = type;
        this.idRecipe = idRecipe;
        this.quantity = quantity;
    }

    public Integer getIdIngredient() {
        return idIngredient;
    }

    public void setIdIngredient(Integer idIngredient) {
        this.idIngredient = idIngredient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getIdRecipe() {
        return idRecipe;
    }

    public void setIdRecipe(int idRecipe) {
        this.idRecipe = idRecipe;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "idIngredient=" + idIngredient +
                ", name='" + name + '\'' +
                ", stock=" + stock +
                ", type='" + type + '\'' +
                ", idRecipe=" + idRecipe +
                ", quantity=" + quantity +
                '}';
    }
}
