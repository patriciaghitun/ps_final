package com.example.ps_proiect2.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ps_proiect2.MainActivity;
import com.example.ps_proiect2.Model.Person;
import com.example.ps_proiect2.Model.Recipe;
import com.example.ps_proiect2.Service.PersonService;
import com.example.ps_proiect2.Service.RecipeService;
import com.example.ps_proiect2.UserActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecipeController {

    public void getRecipeById(final Context context,final Integer givenId,final ListView listView)
    {
        Call<List<Recipe>> persons = RecipeService.getInstance().getRecipes();
        persons.enqueue(new Callback<List<Recipe>>() {
            @Override
            public void onResponse(Call<List<Recipe>> call, Response<List<Recipe>> response) {
                if (response.isSuccessful()) {
                    Log.d("Response", String.valueOf(response.body()));
                    List<Recipe> recipeList = response.body();
                    List<String> recipeNames = new ArrayList<>();
                    for(Recipe r : recipeList){
                        if(r.getIdrecipe()== givenId)
                        recipeNames.add(r.getName());
                    }
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                            context,
                            android.R.layout.simple_list_item_1,
                            recipeNames );

                    listView.setAdapter(arrayAdapter);


                } else {
                    Log.d("Response", "The code is " + response.code());
                }
            }
            @Override
            public void onFailure(Call<List<Recipe>> call, Throwable t) {
                Log.e("Response", "Error in call", t);
            }
        });
    }
    public void getAllRecipes(final Context context,final ListView listView)
    {
        Call<List<Recipe>> recipeList = RecipeService.getInstance().getRecipes();
        recipeList.enqueue(new Callback<List<Recipe>>() {
            @Override
            public void onResponse(Call<List<Recipe>> call, Response<List<Recipe>> response) {
                if(response.isSuccessful()){
                    Log.d("ResponseRECIPE","on response successful");
                    List<String> recipeNames=new ArrayList<>();
                    for(Recipe recipe : response.body()){
                        recipeNames.add(recipe.getName());
                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                                context,
                                android.R.layout.simple_list_item_1,
                                recipeNames );

                        listView.setAdapter(arrayAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Recipe>> call, Throwable t) {
                Log.d("ResponseRECIPE","on failure");
            }
        });
    }

    public void addNewRecipe(final Context context, Recipe newRecipe)
    {
        Call<Recipe> recipeCall = RecipeService.getInstance().addNewRecipe(newRecipe);
        recipeCall.enqueue(new Callback<Recipe>() {

            @Override
            public void onResponse(Call<Recipe> call, Response<Recipe> response) {
                if (response.isSuccessful()) {
                    Log.d("ADD_NEW_RECIPE", "Success. Please login now!");
                    Toast.makeText(context, "Success!", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("ADD_NEW_RECIPE", "Fail");
                    Toast.makeText(context, "wrong data", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Recipe> call, Throwable t) {
                Log.d("ADD_NEW_RECIPE", "onFailure");
            }
        });
    }

    public void deleteById(final Context context, Integer idRecipe) {

        Call<Void> recipe = RecipeService.getInstance().deleteByGivenId(idRecipe);
        recipe.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("DELETE","on response");
                if (response.isSuccessful()) {
                    Log.d("DELETE", "Success. Please login now!");
                    Toast.makeText(context, "Success!", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("DELETE", "Fail");
                    Toast.makeText(context, "Something went wrong :(", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

}
