package com.example.proiect_ps.Controller;

import com.example.proiect_ps.Model.Ingredient;
import com.example.proiect_ps.Model.Person;
import com.example.proiect_ps.Model.Recipe;
import com.example.proiect_ps.Repository.IngredientRepository;
import com.example.proiect_ps.Repository.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/recipe")

public class RecipeController {
    @Autowired
    private RecipeRepository recipeRepository;
    private IngredientRepository ingredientRepository;

    /**
     * Find a specific ingredinet by an id (string) in the body of the request
     * @param idRecipe
     * @return A string response in order to know if the specified ingredient is present or not in the database.
     */
    @PostMapping(path="/findById")
    public String findRecipeById(@RequestBody String idRecipe){
        int id=Integer.parseInt(idRecipe);
        if(recipeRepository.findById(id).isPresent())
        {
            return "Recipe found";
        }else return "Recipe not found";
    }

    @GetMapping(path = "getRecipeById/{idRecipe}")
    public Recipe getRecipeById(@PathVariable Integer idRecipe){
        Recipe recipe = recipeRepository.getRecipesByGivenId(idRecipe);
        return recipe;
    }

    /**
     * Finds all the ingredients present in the database
     * @return List of Iterable Ingredient objects that are present in the database at the time.x
     */
    @GetMapping(path="/all")
    public @ResponseBody Iterable<Recipe> getAllRecipes() {
        return recipeRepository.findAll();
    }

    @GetMapping(path="/findChefByRecipeId/{givenId}")
    public Integer findChefByRecipeId(@PathVariable Integer givenId){
        Integer idChef = recipeRepository.findChefByRecipeId(givenId);
        return idChef;
    }

    @GetMapping(path="/getRecipesByChefId/{idChef}")
    public List<Recipe> getRecipesByChefId(@PathVariable Integer idChef){
        List<Recipe> recipeList = recipeRepository.getRecipesByChefId(idChef);
        return recipeList;
    }

    @PostMapping(path="/add")
    public @ResponseBody
    Recipe addNewRecipe (@RequestBody Recipe newRecipe) {
        return recipeRepository.save(newRecipe);
    }

    @DeleteMapping(path="/deleteById/{idrecipe}")
    public String deleteById(@PathVariable Integer idrecipe){
        if(findRecipeById(idrecipe.toString()).equals("Recipe found"))
        {
            recipeRepository.deleteById(idrecipe);
            return "Recipe deleted";
        }else return "Recipe not found in the database";
    }

    @GetMapping(path = "/getRecipeByName/{name}")
    public Recipe getRecipeByGivenName(@PathVariable String name){
        return recipeRepository.getRecipeByGivenName(name);
    }

    @DeleteMapping(path="/deleteByName/{name}")
    public String deleteByName(@PathVariable String name){
        Recipe recipe = recipeRepository.getRecipeByGivenName(name);
        if(recipe!=null){
           recipeRepository.deleteById(recipe.getIdrecipe());
           return "Recipe deleted";
        }
        else return "Recipe not found";
    }

    @GetMapping(path="/deleteByGivenId/{idrecipe}")
    public @ResponseBody  void deleteByGivenId (@PathVariable Integer idrecipe)
    {
        recipeRepository.deleteById(idrecipe);
    }
}
