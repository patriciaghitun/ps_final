package com.example.proiect_ps.Controller;

import com.example.proiect_ps.Model.Person;
import com.example.proiect_ps.Repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/person")

public class PersonController {
    @Autowired
    private PersonRepository personRepository;

    /**
     * Find a specific ingredinet by an id (string) in the body of the request
     * @param idPerson
     * @return A string response in order to know if the specified ingredient is present or not in the database.
     */
    @PostMapping(path="/findById")
    public String findPersonById(@RequestBody String idPerson){
        int id=Integer.parseInt(idPerson);
        if(personRepository.findById(id).isPresent())
        {
            return "Person found";
        }else return "Person not found";
    }

    /**
     * Finds all the ingredients present in the database
     * @return List of Iterable Ingredient objects that are present in the database at the time.
     */
    @GetMapping(path="/all")
    public @ResponseBody Iterable<Person> getAllPersons() {
        return personRepository.findAll();
    }

    @GetMapping(path = "getPersonByUsername/{username}")
    public Person getPersonByUsername(@PathVariable String username){
        Person person = personRepository.getPersonByUsername(username);
        return person;
    }

    @GetMapping(path = "login/{username}/{password}/{role}")
    public Person login(@PathVariable String username, @PathVariable String password, @PathVariable String role){
        Person person = personRepository.login(username, password, role);
        return person;
    }

    @PostMapping(path="/add")
    public @ResponseBody Person addNewPerson (@RequestBody Person newPerson) {
        return personRepository.save(newPerson);
    }
}
