package com.example.proiect_ps.Model;

public class PersonFactory {

    public static PersonType createPerson(String type){
        if(type.equals(PersonType.USER)){
            return new User();
        }else if(type.equals(PersonType.CHEF)){
            return new Chef();
        }else{
            return null;
        }
    }
}
