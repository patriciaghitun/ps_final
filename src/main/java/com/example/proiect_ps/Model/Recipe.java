package com.example.proiect_ps.Model;

import javax.persistence.*;
import java.util.Observable;

@Entity
public class Recipe extends Observable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idrecipe;
    private String name;
    private int idchef;

    private String description;

    public Recipe(){}

    public Recipe(Integer idrecipe, String name, String description, int idchef) {
        this.idrecipe = idrecipe;
        this.name = name;
        this.description = description;
        this.idchef = idchef;
    }

    public Integer getIdrecipe() {
        return idrecipe;
    }

    public void setIdrecipe(Integer idrecipe) {
        this.idrecipe = idrecipe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIdchef() {
        return idchef;
    }

    public void setIdchef(int idchef) {
        this.idchef = idchef;
    }
}
