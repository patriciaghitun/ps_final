package com.example.proiect_ps.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Observable;
import java.util.Observer;

@Entity
public class Person implements Observer{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idperson;

    private String username;
    private String password;
    private String role; //bucatar/user

    public Person(){}

    public Person(Integer idperson, String username, String password, String role) {
        this.idperson = idperson;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public Person(String username, String password, String role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public Integer getIdperson() {
        return idperson;
    }

    public void setIdperson(Integer idperson) {
        this.idperson = idperson;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String name) {
        this.username = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Hei " + this.getIdperson() + " someone cooked " + ((Recipe)o).getName() + "<->"+arg);
    }
}
