package com.example.proiect_ps.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Observable;

@Entity
public class Ingredient {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idingredient;
    private String name;
    private String type;
    private int idrecipe;

    public Ingredient(){}

    public Ingredient(Integer idingredient, String name, String type, int idrecipe) {
        this.idingredient = idingredient;
        this.name = name;
        this.type = type;
        this.idrecipe = idrecipe;
    }



    public Integer getIdingredient() {
        return idingredient;
    }

    public void setIdingredient(Integer idingredient) {
        this.idingredient = idingredient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getIdrecipe() {
        return idrecipe;
    }

    public void setIdrecipe(int idrecipe) {
        this.idrecipe = idrecipe;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "idingredient=" + idingredient +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", idrecipe=" + idrecipe +
                '}';
    }
}


