package com.example.proiect_ps.Model;

import java.util.Observable;
import java.util.Observer;

public class Chef extends Person implements PersonType {
    private int nrRecipes;

    public Chef(){}

    public Chef(Integer idperson, String username,String pass, int nrRecipes) {
        super(idperson,username,pass,"chef");
        this.nrRecipes = nrRecipes;
    }

    /*
    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Hei " + this.getIdperson() + " someone cooked " + o + "<->"+arg);
    }
    */
}
